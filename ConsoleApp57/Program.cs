﻿using System;

namespace ConsoleApp57
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Creat  Random  Array and  Get The Arthimetic Average

            int count = 10;
            double sum = 0;
            double average = 0;

            int[] arr = new int[count];
            Random rnd = new Random();

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(10, 50);
                sum += arr[i];
                average = sum / arr.Length;
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();
            Console.WriteLine("--------------");
            Console.WriteLine("average=" + average);

            #endregion

            #region if ( Average < 10) Get Sum of Array

            if (average < 10)
            {
                int sumArray = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    sumArray += arr[i];
                }
                Console.WriteLine();
                Console.WriteLine("sumArray=" + sumArray);
            }
            #endregion

            #region if (average > 10 && average < 20) Get Max of Array

            else if (average > 10 && average < 20)
            {
                int max = arr[0];
                for (int i = 0; i < arr.Length; i++)
                {
                    {
                        if (max < arr[i])
                            max = arr[i];
                    }
                }
                Console.WriteLine();
                Console.WriteLine("Max number of Array=" + max);
            }

            #endregion

            #region if ( Average > 20 && Average < 30 ) Get Sum of Even Numbers

            else if (average > 20 && average < 30)
            {
                int sumEven = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i] % 2 == 0)
                        sumEven += arr[i];
                }
                Console.WriteLine();
                Console.WriteLine("Sum of Even Numbers=" + sumEven);
            }

            #endregion

            #region if ( Average > 30 && Average < 40 ) Get Min After Max

            else if (average > 30 && average < 40)
            {
                int max = arr[0];
                int maxIndex = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    if (max < arr[i])
                    {
                        max = arr[i];
                        maxIndex = i;
                    }
                }
                int min = arr[maxIndex + 1];
                for (int i = maxIndex + 2; i < arr.Length; i++)
                {
                    if (min > arr[i])
                        min = arr[i];
                }
                Console.WriteLine();
                Console.WriteLine("Min After Max=" + min);
            }

            #endregion

            #region if ( Average > 40 && Average < 50 ) Get Max Value of Numbers Which Have odd Index

            else if (average > 40 && average < 50)
            {
                int max = arr[1];
                for (int i = 0; i < arr.Length; i++)
                {
                    if (i % 2 != 0)
                    {
                        if (max < arr[i])
                            max = arr[i];
                    }
                }
                Console.WriteLine();
                Console.WriteLine("Max Value of Numbers Which Have odd Index=" + max);
            }

            #endregion

            #region if ( Average > 50 ) Get Max Value of Even Numbers

            else if(average > 50)
            {
                int maxEven = arr[0];
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i] % 2 == 0)
                    {
                        if (maxEven < arr[i])
                            maxEven = arr[i];
                    }
                }
                Console.WriteLine();
                Console.WriteLine("Max Value of Even Numbers=" + maxEven);
            }

            #endregion

        }
    }
}
